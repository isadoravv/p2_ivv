# P2_vuongvan_isadora : Transformez une maquette en site web

https://isadoravv.gitlab.io/p2_ivv/

## Demo

![Gif Demo](./demo.gif)

## Development server

Run `npm run dev` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

Files reformatted by Prettier automatically on commit.
HTML automatically validated on push (html-validate).

## Choix faits

Testé sur Chrome
Google font
SVG
Fichier CSS non découpé
Fontawesome laissé comme ça
